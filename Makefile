SHELL := /bin/bash

include workflow/config.mk

databases: sampleSheet.txt workflow/env.yaml
	@echo '## Building databases'
	source $$MODULESHOME/init/bash; module load snakemake; snakemake --jobs $(JOBS) --snakefile workflow/Snakefile --use-envmodules --local-cores $(CORES) --cluster 'module load slurm; SlurmEasy --email --name "{rule}.{wildcards.sample}" --threads {threads}' 2>&1 | tee snakemake.log
	@echo '## Done. Summary:'
	@cat summary.txt
	@cat summary.txt <(echo -e "\n# Snakemake log:\n") snakemake.log | mailx -s 'Blast database pipeline finished' $(ADRESSEE)

install:
	rsync --archive --update --verbose --compress --partial --progress blastdb/* $(PREFIX)
	@cat summary.txt | mailx -s 'Blast databases installed' $(ADRESSEE)

clean:
	@echo '## Cleaning up'
	mkdir --parents old_sample_sheets old_summaries
	if [[ -f sampleSheet.txt ]]; then mv --interactive sampleSheet.txt old_sample_sheets/sampleSheet.$$(date --iso-8601); fi
	if [[ -f summary.txt ]]; then mv -i summary.txt old_summaries/summary.$$(date --iso-8601).txt; fi
	rm --recursive --force blastdb renamed trinity *.err *.out
	@echo '## Archiving'
	rsync --archive --update --verbose --exclude ".snakemake" --exclude ".git" ./ /data/boehm/group2/petersen/projects/blast_databases/

.PHONY: sampleSheet.txt workflow/env.yaml
