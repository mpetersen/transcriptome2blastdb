##############################################################################
# Make BLAST database(s) of Trinity assemblies from transcriptomic NGS datasets
#
# This workflow expects a sample sheet in the current directory. It must be
# named "sampleSheet.txt".
#
# The sample sheet has the following format:
#
# /path/to/file.fastq.gz        sampleName
#
# If multiple files have the same sample name they'll all get merged together.
#
# Read 1 and 2 are distinguished by ending in _R1.fastq.gz and such
##############################################################################

read_files = { "R1": dict(), "R2": dict() }
samples = [ ]
with open("sampleSheet.txt", "r") as sampleSheet:
    for line in sampleSheet:
        fname, sampleName = line.strip().split("\t")
        # add to samples list
        if sampleName not in samples:
            samples.append(sampleName)
        # add files to files lists
        if sampleName not in read_files["R2"]:
            read_files["R1"][sampleName] = []
            read_files["R2"][sampleName] = []
        if fname.find("_R1.") != -1:
            read_files["R1"][sampleName].append(fname)
        elif fname.find("_R2.") != -1:
            read_files["R2"][sampleName].append(fname)
        else:
            raise Exception("Could not find either read 1 (\"_R1.\") or read 2 (\"_R2.\") in file name: {}".format(fname))

stats_files = expand("blastdb/{sample}_trinity/stats.txt", sample = samples)
md5sums_files = expand("blastdb/{sample}_trinity/md5sums.txt", sample = samples)
summary = "summary.txt"

rule all:
    input:
        expand("blastdb/{sample}_trinity/finished", sample = samples),
        stats_files,
        md5sums_files,
        summary

rule trinity:
    input:
        left  = lambda wildcards: read_files["R1"][wildcards.sample],
        right = lambda wildcards: read_files["R2"][wildcards.sample]
    output:
        "trinity/{sample}_trinity/Trinity.fasta"
    log:
        'logs/trinity/{sample}_trinity.trinity.log'
    benchmark:
        "benchmarks/{sample}_trinity.trinity.benchmark.txt"
    envmodules:
        "Trinity/2.8.5"
    conda:
        "env.yaml"
    params:
        extra = "--trimmomatic --NO_SEQTK"
    threads:
        32
    resources:
        mem_gb = 160
    wrapper:
        "0.76.0/bio/trinity"

localrules: add_sample_to_header
rule add_sample_to_header:
    input:
        "trinity/{sample}_trinity/Trinity.fasta"
    output:
        "renamed/{sample}_trinity.fasta"
    shell:
        """
        sed -e '/^>/s/^>/>{wildcards.sample}_/' -e 's/path=.\+$//' {input} > {output}
        """

rule makedb:
    input:
        "renamed/{sample}_trinity.fasta"
    output:
        "blastdb/{sample}_trinity/finished"
    log:
        "blastdb/{sample}_trinity/{sample}_trinity.makeblastdb.log"
    benchmark:
        "benchmarks/{sample}_trinity.makeDB.benchmark.txt"
    envmodules:
        "blast/2.9.0"
    threads: 2
    params:
        dbtype = "nucl",
        extra = "-parse_seqids"
    shell:
        """
        makeblastdb \
            -dbtype {params.dbtype} \
            {params.extra} \
            -in {input} \
            -out "blastdb/{wildcards.sample}_trinity/{wildcards.sample}_trinity" \
            -title "{wildcards.sample}_trinity" \
            -logfile {log}
        touch {output}
        """


localrules: stats
rule stats:
    input:
        "blastdb/{sample}_trinity/finished"
    output:
        "blastdb/{sample}_trinity/stats.txt"
    benchmark:
        "benchmarks/{sample}_trinity.stats.benchmark.txt"
    envmodules:
        "blast/2.9.0"
    shell:
        """
        blastdbcmd -db $(dirname {input})/{wildcards.sample}_trinity -info > {output}
        """

localrules: md5sum
rule md5sum:
    input:
        "blastdb/{sample}_trinity/stats.txt"
    output:
        "blastdb/{sample}_trinity/md5sums.txt"
    shell:
        """
        (cd $(dirname {input}); md5sum * > md5sums.txt)
        """

localrules: summary
rule summary:
    input:
        stats_files
    output:
        summary
    shell:
        """
        head -n 4 {input} | sed -e '/^==>/d' -e 's/^\(Database.\+\)$/# \\1\\n/' > {output}
        """
