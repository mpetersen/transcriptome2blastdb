# Where should the databases be installed by `make install`?
PREFIX := /data/boehm/blast_databases

# How many parallel jobs may be started on the cluster? Note that the fastp and
# bbmerge rules use 10 CPUs for each sample, so 6 jobs times 10 CPUs will be 60
# slots on the cluster in parallel.
JOBS := 6

# How many local jobs can the pipeline run in parallel? Note that this is only
# used for local rules, not the ones run on the cluster.
CORES := 6

# Where should the confirmation email go? If to a person at the MPI-IE, no need
# to add the @domain.tld part (is assumed automatically)
ADRESSEE := petersen
